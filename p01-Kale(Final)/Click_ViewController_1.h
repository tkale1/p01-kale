//
//  Click_ViewController_1.h
//  p01-Kale(Final)
//
//  Created by Tanmay Kale on 2/1/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Click_ViewController_1 : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *clickLabel; //defining label for Click Button View.

@property (strong, nonatomic) IBOutlet UIButton *clickButton; //defining Button for Click Button View.

@end
