//
//  AppDelegate.h
//  p01-Kale(Final)
//
//  Created by Tanmay Kale on 2/1/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

