//
//  Click_ViewController_1.m
//  p01-Kale(Final)
//
//  Created by Tanmay Kale on 2/1/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import "Click_ViewController_1.h"

@interface Click_ViewController_1 ()

@end

@implementation Click_ViewController_1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)clickButton:(id)sender {
    _clickLabel.text = @" This Button thing actually works..!! :P ";
}/* defining Button function for changing label  text.*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
