//
//  Slide_ViewController2.m
//  p01-Kale(Final)
//
//  Created by Tanmay Kale on 2/1/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import "Slide_ViewController2.h"

@interface Slide_ViewController2 ()

@end

@implementation Slide_ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)slideButton:(id)sender {
    int fonts = (int)(_slideButton.value);
    //NSString *newtext= [NSString stringWithFormat:@"%d",fonts];
    _slideLabel.font=[UIFont systemFontOfSize:fonts];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
