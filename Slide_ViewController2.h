//
//  Slide_ViewController2.h
//  p01-Kale(Final)
//
//  Created by Tanmay Kale on 2/1/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Slide_ViewController2 : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *slideLabel; // defining Label.


@property (strong, nonatomic) IBOutlet UISlider *slideButton; // defining Slider variable
@end
